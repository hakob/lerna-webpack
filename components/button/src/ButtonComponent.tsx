import React from 'react';

import { ButtonComponentProps } from './ButtonComponent.types';
const ButtonComponent: React.FC<ButtonComponentProps> = () => (
  <div>
    <h1 className="heading">I'm the test component</h1>
  </div>
);

export default ButtonComponent;

const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = {
  entry: './src/index.ts',
  // mode: 'production',
  output: {
    path: path.resolve('dist'),
    filename: '[name].js',
    library: 'react-component-library',
    libraryTarget: 'umd',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@thedesignsystem/button': path.resolve(
        __dirname,
        '../components/button/src/index',
      ),
      '@thedesignsystem/buttonnew': path.resolve(
        __dirname,
        '../components/buttonnew/src/index',
      ),
    },
  },
  module: {
    rules: [
      {
        test: /\.css/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.tsx?$/,
        use: ['babel-loader', 'ts-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader',
        options: {
          fallback: 'file-loader',
          limit: Infinity,
        },
      },
    ],
  },
  externals: {
    react: 'react',
    'react-dom': 'react-dom',
  },
  plugins: [
    new ESLintPlugin({
      files: 'src/**/*.ts',
    }),
  ],
};
